
# Transparentify

Transparentify is a tool to remove images background.

### Installation
#### Manual install on a fresh ubuntu 
* Add basic 
`sudo apt update`
`sudo apt install software-properties-common build-essential`
* Install Homebrew
`/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"`
`echo 'eval $(/home/linuxbrew/.linuxbrew/bin/brew shellenv)' >> /home/ubuntu/.profile`
`eval $(/home/linuxbrew/.linuxbrew/bin/brew shellenv)`
<!-- 
* Install python3.7
`brew install python@3.7`
`echo 'export PATH="/home/linuxbrew/.linuxbrew/opt/python@3.7/bin:$PATH"' >> ~/.profile`
`export LDFLAGS="-L/home/linuxbrew/.linuxbrew/opt/python@3.7/lib"`
`export CPPFLAGS="-I/home/linuxbrew/.linuxbrew/opt/python@3.7/include"` -->

* Update python to 3.7
`https://dev.to/serhatteker/how-to-upgrade-to-python-3-7-on-ubuntu-18-04-18-10-5hab`

* Install pip
`sudo apt install python3-pip`
* Clone repo
`git clone https://ashkaps@bitbucket.org/kashifaAfrin/transparentify.git`
* change dir
`cd transparentify`

* Install CUDA
`brew install Caskroom/cask/cuda`

* Create virtual environment
`sudo apt install python3.7-venv`
`python3.7 -m venv rembgenv`
`source rembgenv/bin/activate`

* install required python packages
`python3.7 -m pip install -r requirements.txt`
* Change dir to the api server file
`cd src/rembg`
* Run the flask app
`python3.7 server.py`

#### Docker
* Install docker
https://docs.docker.com/engine/install/ubuntu/
* change dir
`cd transparentify`
* Build docker image
`docker build -t rembg .`
* Run docker image
`docker run -p 5000:5000 rembg`




### License



#### Troubleshoot
`UserWarning: CUDA initialization: Found no NVIDIA driver on your system.`
https://medium.com/@sargupta/fix-nvidia-smi-not-working-3c040ac426b3