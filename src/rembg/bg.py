import io

import numpy as np
from PIL import Image
# from pymatting.alpha.estimate_alpha_cf import estimate_alpha_cf
# from pymatting.foreground.estimate_foreground_ml import estimate_foreground_ml
# from pymatting.alpha.estimate_alpha_knn import estimate_alpha_knn
# from pymatting.foreground.estimate_foreground_ml import estimate_foreground_ml
from pymatting.alpha.estimate_alpha_rw import estimate_alpha_rw
from pymatting.foreground.estimate_foreground_cf import estimate_foreground_cf
from pymatting.util.util import stack_images
from scipy.ndimage.morphology import binary_erosion

from u2net import detect

model_u2net = detect.load_model(model_name="u2net")
model_u2netp = detect.load_model(model_name="u2netp")


def alpha_matting_cutout(
    img, mask, foreground_threshold, background_threshold, erode_structure_size,
):
    base_size = (1000, 1000)
    size = img.size

    img.thumbnail(base_size, Image.BILINEAR)
    mask = mask.resize(img.size, Image.BILINEAR)

    img = np.asarray(img)
    mask = np.asarray(mask)

    # guess likely foreground/background
    is_foreground = mask > foreground_threshold
    is_background = mask < background_threshold

    # erode foreground/background
    structure = None
    if erode_structure_size > 0:
        structure = np.ones((erode_structure_size, erode_structure_size), dtype=np.int)

    is_foreground = binary_erosion(is_foreground, structure=structure)
    is_background = binary_erosion(is_background, structure=structure, border_value=1)

    # build trimap
    # 0   = background
    # 128 = unknown
    # 255 = foreground
    trimap = np.full(mask.shape, dtype=np.uint8, fill_value=128)
    trimap[is_foreground] = 255
    trimap[is_background] = 0

    # build the cutout image
    img_normalized = img / 255.0
    trimap_normalized = trimap / 255.0

    alpha = estimate_alpha_rw(img_normalized, trimap_normalized)
    foreground = estimate_foreground_cf(img_normalized, alpha)
    cutout = stack_images(foreground, alpha)

    cutout = np.clip(cutout * 255, 0, 255).astype(np.uint8)
    cutout = Image.fromarray(cutout)
    cutout = cutout.resize(size, Image.BILINEAR)

    return cutout


def naive_cutout(img, mask):
    empty = Image.new("RGBA", (img.size), 0)
    cutout = Image.composite(img, empty, mask.resize(img.size, Image.BILINEAR))
    return cutout


def remove(
    data,
    model_name="u2net",
    alpha_matting=True,
    alpha_matting_foreground_threshold=240 ,
    alpha_matting_background_threshold=10,
    alpha_matting_erode_structure_size=3,
):
    model = model_u2net

    if model == "u2netp":
        model = model_u2netp

    img = Image.open(io.BytesIO(data)).convert("RGB")
    mask = detect.predict(model, np.array(img)).convert("L")
#     Image._show(mask)
    pixel = list(mask.getdata())
#     new_pixel =[]
#     i=0
#     for x in pixel:
#         #         print(x)
#         if x <160 and x>100:
#             new_pixel.append(255)
#         else:
#             new_pixel.append(x)
#         
#         print(" original val="+str(x)+"new value="+str(new_pixel[i]))
#         i = i+1
#     print('----------------------------------------')

    WIDTH, HEIGHT = mask.size        
    data = [pixel[offset:offset+WIDTH] for offset in range(0, WIDTH*HEIGHT, WIDTH)]
    # At this point the image's pixels are all in memory and can be accessed
    # individually using data[row][col].
    
    # For example:
#     new_pixel = list()
#     count=0
#     for row in data:
#         new_row = list()
#         for x in row:
#             if x <15 and x>1:
#                 new_row.append(255)
#                 count=count+1
#             else:
#                 new_row.append(x)
#         new_pixel.append(new_row)
    
#     if count > (data.__sizeof__()/2):
#         
#         
#         array = np.array(new_pixel, dtype=np.uint8)
#         new_image = Image.fromarray(array)
#         Image._show(new_image)
#         mask = new_image
    

    if alpha_matting:
        cutout = alpha_matting_cutout(
            img,
            mask,
            alpha_matting_foreground_threshold,
            alpha_matting_background_threshold,
            alpha_matting_erode_structure_size,
        )
    else:
        cutout = naive_cutout(img, mask)

    bio = io.BytesIO()
    cutout.save(bio, "PNG")

    return bio.getbuffer()
